# CONTENTS

人喰い転移者の異世界復讐譚　～無能はスキル『捕食』で成り上がる～  
食人转移者的异世界复仇谭  
喰人转移者的异世界复仇谭  

作者： kiki  



- :closed_book: [README.md](README.md) - 簡介與其他資料
- :pencil: [整合樣式](%E6%95%B4%E5%90%88%E6%A8%A3%E5%BC%8F.md)
- [含有原文的章節](ja.md) - 可能為未翻譯或者吞樓，等待圖轉文之類
- [待修正屏蔽字](%E5%BE%85%E4%BF%AE%E6%AD%A3%E5%B1%8F%E8%94%BD%E5%AD%97.md) - 需要有人協助將 `**` 內的字補上
- [格式與譯名整合樣式](https://github.com/bluelovers/node-novel/blob/master/lib/locales/%E4%BA%BA%E5%96%B0%E3%81%84%E8%BB%A2%E7%A7%BB%E8%80%85%E3%81%AE%E7%95%B0%E4%B8%96%E7%95%8C%E5%BE%A9%E8%AE%90%E8%AD%9A%E3%80%80%EF%BD%9E%E7%84%A1%E8%83%BD%E3%81%AF%E3%82%B9%E3%82%AD%E3%83%AB%E3%80%8E%E6%8D%95%E9%A3%9F%E3%80%8F%E3%81%A7%E6%88%90%E3%82%8A%E4%B8%8A%E3%81%8C%E3%82%8B%EF%BD%9E.ts) - 如果連結錯誤 請點[這裡](https://github.com/bluelovers/node-novel/blob/master/lib/locales/)
-  :heart: [EPUB](https://gitlab.com/demonovel/epub-txt/blob/master/ts/%E4%BA%BA%E5%96%B0%E3%81%84%E8%BB%A2%E7%A7%BB%E8%80%85%E3%81%AE%E7%95%B0%E4%B8%96%E7%95%8C%E5%BE%A9%E8%AE%90%E8%AD%9A%E3%80%80%EF%BD%9E%E7%84%A1%E8%83%BD%E3%81%AF%E3%82%B9%E3%82%AD%E3%83%AB%E3%80%8E%E6%8D%95%E9%A3%9F%E3%80%8F%E3%81%A7%E6%88%90%E3%82%8A%E4%B8%8A%E3%81%8C%E3%82%8B%EF%BD%9E.epub) :heart:  ／ [TXT](https://gitlab.com/demonovel/epub-txt/blob/master/ts/out/%E4%BA%BA%E5%96%B0%E3%81%84%E8%BB%A2%E7%A7%BB%E8%80%85%E3%81%AE%E7%95%B0%E4%B8%96%E7%95%8C%E5%BE%A9%E8%AE%90%E8%AD%9A%E3%80%80%EF%BD%9E%E7%84%A1%E8%83%BD%E3%81%AF%E3%82%B9%E3%82%AD.out.txt) - 如果連結錯誤 請點[這裡](https://gitlab.com/demonovel/epub-txt/blob/master/ts/)
- :mega: [https://discord.gg/MnXkpmX](https://discord.gg/MnXkpmX) - 報錯交流群，如果已經加入請點[這裡](https://discordapp.com/channels/467794087769014273/467794088285175809) 或 [Discord](https://discordapp.com/channels/@me)


![導航目錄](https://chart.apis.google.com/chart?cht=qr&chs=150x150&chl=https://gitlab.com/novel-group/txt-source/blob/master/ts_out/人喰い転移者の異世界復讐譚　～無能はスキル『捕食』で成り上がる～/導航目錄.md "導航目錄")




## [1章　殺戮の王都](00000_1%E7%AB%A0%E3%80%80%E6%AE%BA%E6%88%AE%E3%81%AE%E7%8E%8B%E9%83%BD)

- [1　異世界召喚伴隨失望](00000_1%E7%AB%A0%E3%80%80%E6%AE%BA%E6%88%AE%E3%81%AE%E7%8E%8B%E9%83%BD/1%E3%80%80%E7%95%B0%E4%B8%96%E7%95%8C%E5%8F%AC%E5%96%9A%E4%BC%B4%E9%9A%A8%E5%A4%B1%E6%9C%9B.txt)
- [2　阿尼瑪的覺醒](00000_1%E7%AB%A0%E3%80%80%E6%AE%BA%E6%88%AE%E3%81%AE%E7%8E%8B%E9%83%BD/2%E3%80%80%E9%98%BF%E5%B0%BC%E7%91%AA%E7%9A%84%E8%A6%BA%E9%86%92.txt)
- [3　向奈落更深處](00000_1%E7%AB%A0%E3%80%80%E6%AE%BA%E6%88%AE%E3%81%AE%E7%8E%8B%E9%83%BD/3%E3%80%80%E5%90%91%E5%A5%88%E8%90%BD%E6%9B%B4%E6%B7%B1%E8%99%95.txt)
- [4　狩獵的時間](00000_1%E7%AB%A0%E3%80%80%E6%AE%BA%E6%88%AE%E3%81%AE%E7%8E%8B%E9%83%BD/4%E3%80%80%E7%8B%A9%E7%8D%B5%E7%9A%84%E6%99%82%E9%96%93.txt)
- [5　『捕食』](00000_1%E7%AB%A0%E3%80%80%E6%AE%BA%E6%88%AE%E3%81%AE%E7%8E%8B%E9%83%BD/5%E3%80%80%E3%80%8E%E6%8D%95%E9%A3%9F%E3%80%8F.txt)
- [6　願、他在死後也永受痛苦](00000_1%E7%AB%A0%E3%80%80%E6%AE%BA%E6%88%AE%E3%81%AE%E7%8E%8B%E9%83%BD/6%E3%80%80%E9%A1%98%E3%80%81%E4%BB%96%E5%9C%A8%E6%AD%BB%E5%BE%8C%E4%B9%9F%E6%B0%B8%E5%8F%97%E7%97%9B%E8%8B%A6.txt)
- [7　老饕編劇與小丑們](00000_1%E7%AB%A0%E3%80%80%E6%AE%BA%E6%88%AE%E3%81%AE%E7%8E%8B%E9%83%BD/7%E3%80%80%E8%80%81%E9%A5%95%E7%B7%A8%E5%8A%87%E8%88%87%E5%B0%8F%E4%B8%91%E5%80%91.txt)
- [8　快樂的籠絡時間](00000_1%E7%AB%A0%E3%80%80%E6%AE%BA%E6%88%AE%E3%81%AE%E7%8E%8B%E9%83%BD/8%E3%80%80%E5%BF%AB%E6%A8%82%E7%9A%84%E7%B1%A0%E7%B5%A1%E6%99%82%E9%96%93.txt)
- [9　處刑前夜](00000_1%E7%AB%A0%E3%80%80%E6%AE%BA%E6%88%AE%E3%81%AE%E7%8E%8B%E9%83%BD/9%E3%80%80%E8%99%95%E5%88%91%E5%89%8D%E5%A4%9C.txt)
- [10　斷頭台上的喜劇演出](00000_1%E7%AB%A0%E3%80%80%E6%AE%BA%E6%88%AE%E3%81%AE%E7%8E%8B%E9%83%BD/10%E3%80%80%E6%96%B7%E9%A0%AD%E5%8F%B0%E4%B8%8A%E7%9A%84%E5%96%9C%E5%8A%87%E6%BC%94%E5%87%BA.txt)
- [11　虐殺日和](00000_1%E7%AB%A0%E3%80%80%E6%AE%BA%E6%88%AE%E3%81%AE%E7%8E%8B%E9%83%BD/11%E3%80%80%E8%99%90%E6%AE%BA%E6%97%A5%E5%92%8C.txt)
- [12　這個世界不存在正義的伙伴](00000_1%E7%AB%A0%E3%80%80%E6%AE%BA%E6%88%AE%E3%81%AE%E7%8E%8B%E9%83%BD/12%E3%80%80%E9%80%99%E5%80%8B%E4%B8%96%E7%95%8C%E4%B8%8D%E5%AD%98%E5%9C%A8%E6%AD%A3%E7%BE%A9%E7%9A%84%E4%BC%99%E4%BC%B4.txt)
- [13　強欲、自帝國而來](00000_1%E7%AB%A0%E3%80%80%E6%AE%BA%E6%88%AE%E3%81%AE%E7%8E%8B%E9%83%BD/13%E3%80%80%E5%BC%B7%E6%AC%B2%E3%80%81%E8%87%AA%E5%B8%9D%E5%9C%8B%E8%80%8C%E4%BE%86.txt)
- [14　惡意披上愛的外衣](00000_1%E7%AB%A0%E3%80%80%E6%AE%BA%E6%88%AE%E3%81%AE%E7%8E%8B%E9%83%BD/14%E3%80%80%E6%83%A1%E6%84%8F%E6%8A%AB%E4%B8%8A%E6%84%9B%E7%9A%84%E5%A4%96%E8%A1%A3.txt)
- [15　共犯者](00000_1%E7%AB%A0%E3%80%80%E6%AE%BA%E6%88%AE%E3%81%AE%E7%8E%8B%E9%83%BD/15%E3%80%80%E5%85%B1%E7%8A%AF%E8%80%85.txt)
- [16　初次殺人](00000_1%E7%AB%A0%E3%80%80%E6%AE%BA%E6%88%AE%E3%81%AE%E7%8E%8B%E9%83%BD/16%E3%80%80%E5%88%9D%E6%AC%A1%E6%AE%BA%E4%BA%BA.txt)
- [17 　終焉的扳機](00000_1%E7%AB%A0%E3%80%80%E6%AE%BA%E6%88%AE%E3%81%AE%E7%8E%8B%E9%83%BD/17%20%E3%80%80%E7%B5%82%E7%84%89%E7%9A%84%E6%89%B3%E6%A9%9F.txt)
- [18 　人喰い](00000_1%E7%AB%A0%E3%80%80%E6%AE%BA%E6%88%AE%E3%81%AE%E7%8E%8B%E9%83%BD/18%20%E3%80%80%E4%BA%BA%E5%96%B0%E3%81%84.txt)
- [19　然後復仇譚拉開帷幕](00000_1%E7%AB%A0%E3%80%80%E6%AE%BA%E6%88%AE%E3%81%AE%E7%8E%8B%E9%83%BD/19%E3%80%80%E7%84%B6%E5%BE%8C%E5%BE%A9%E4%BB%87%E8%AD%9A%E6%8B%89%E9%96%8B%E5%B8%B7%E5%B9%95.txt)


## [2章 　正しい天使の育て方](00010_2%E7%AB%A0%20%E3%80%80%E6%AD%A3%E3%81%97%E3%81%84%E5%A4%A9%E4%BD%BF%E3%81%AE%E8%82%B2%E3%81%A6%E6%96%B9)

- [20　殺人者們的秘密約會](00010_2%E7%AB%A0%20%E3%80%80%E6%AD%A3%E3%81%97%E3%81%84%E5%A4%A9%E4%BD%BF%E3%81%AE%E8%82%B2%E3%81%A6%E6%96%B9/20%E3%80%80%E6%AE%BA%E4%BA%BA%E8%80%85%E5%80%91%E7%9A%84%E7%A7%98%E5%AF%86%E7%B4%84%E6%9C%83.txt)
- [21　霧中蹂躙](00010_2%E7%AB%A0%20%E3%80%80%E6%AD%A3%E3%81%97%E3%81%84%E5%A4%A9%E4%BD%BF%E3%81%AE%E8%82%B2%E3%81%A6%E6%96%B9/21%E3%80%80%E9%9C%A7%E4%B8%AD%E8%B9%82%E8%BA%99.txt)
- [22　搖擺的心](00010_2%E7%AB%A0%20%E3%80%80%E6%AD%A3%E3%81%97%E3%81%84%E5%A4%A9%E4%BD%BF%E3%81%AE%E8%82%B2%E3%81%A6%E6%96%B9/22%E3%80%80%E6%90%96%E6%93%BA%E7%9A%84%E5%BF%83.txt)
- [23　馬車從城鎮駛往地獄](00010_2%E7%AB%A0%20%E3%80%80%E6%AD%A3%E3%81%97%E3%81%84%E5%A4%A9%E4%BD%BF%E3%81%AE%E8%82%B2%E3%81%A6%E6%96%B9/23%E3%80%80%E9%A6%AC%E8%BB%8A%E5%BE%9E%E5%9F%8E%E9%8E%AE%E9%A7%9B%E5%BE%80%E5%9C%B0%E7%8D%84.txt)
- [24　缺肢癖](00010_2%E7%AB%A0%20%E3%80%80%E6%AD%A3%E3%81%97%E3%81%84%E5%A4%A9%E4%BD%BF%E3%81%AE%E8%82%B2%E3%81%A6%E6%96%B9/24%E3%80%80%E7%BC%BA%E8%82%A2%E7%99%96.txt)
- [25　復讐鬼と健常者の相互理解](00010_2%E7%AB%A0%20%E3%80%80%E6%AD%A3%E3%81%97%E3%81%84%E5%A4%A9%E4%BD%BF%E3%81%AE%E8%82%B2%E3%81%A6%E6%96%B9/25%E3%80%80%E5%BE%A9%E8%AE%90%E9%AC%BC%E3%81%A8%E5%81%A5%E5%B8%B8%E8%80%85%E3%81%AE%E7%9B%B8%E4%BA%92%E7%90%86%E8%A7%A3.txt)
- [26　越是美麗之物越想將其破壞](00010_2%E7%AB%A0%20%E3%80%80%E6%AD%A3%E3%81%97%E3%81%84%E5%A4%A9%E4%BD%BF%E3%81%AE%E8%82%B2%E3%81%A6%E6%96%B9/26%E3%80%80%E8%B6%8A%E6%98%AF%E7%BE%8E%E9%BA%97%E4%B9%8B%E7%89%A9%E8%B6%8A%E6%83%B3%E5%B0%87%E5%85%B6%E7%A0%B4%E5%A3%9E.txt)
- [27　注定滅亡](00010_2%E7%AB%A0%20%E3%80%80%E6%AD%A3%E3%81%97%E3%81%84%E5%A4%A9%E4%BD%BF%E3%81%AE%E8%82%B2%E3%81%A6%E6%96%B9/27%E3%80%80%E6%B3%A8%E5%AE%9A%E6%BB%85%E4%BA%A1.txt)
- [28　兎狩り](00010_2%E7%AB%A0%20%E3%80%80%E6%AD%A3%E3%81%97%E3%81%84%E5%A4%A9%E4%BD%BF%E3%81%AE%E8%82%B2%E3%81%A6%E6%96%B9/28%E3%80%80%E5%85%8E%E7%8B%A9%E3%82%8A.txt)
- [29　鴨が葱を背負って飛んでくる](00010_2%E7%AB%A0%20%E3%80%80%E6%AD%A3%E3%81%97%E3%81%84%E5%A4%A9%E4%BD%BF%E3%81%AE%E8%82%B2%E3%81%A6%E6%96%B9/29%E3%80%80%E9%B4%A8%E3%81%8C%E8%91%B1%E3%82%92%E8%83%8C%E8%B2%A0%E3%81%A3%E3%81%A6%E9%A3%9B%E3%82%93%E3%81%A7%E3%81%8F%E3%82%8B.txt)
- [30　酩酊大醉的（自稱）主人公們](00010_2%E7%AB%A0%20%E3%80%80%E6%AD%A3%E3%81%97%E3%81%84%E5%A4%A9%E4%BD%BF%E3%81%AE%E8%82%B2%E3%81%A6%E6%96%B9/30%E3%80%80%E9%85%A9%E9%85%8A%E5%A4%A7%E9%86%89%E7%9A%84%EF%BC%88%E8%87%AA%E7%A8%B1%EF%BC%89%E4%B8%BB%E4%BA%BA%E5%85%AC%E5%80%91.txt)
- [31　英雄，又名實驗小豚鼠](00010_2%E7%AB%A0%20%E3%80%80%E6%AD%A3%E3%81%97%E3%81%84%E5%A4%A9%E4%BD%BF%E3%81%AE%E8%82%B2%E3%81%A6%E6%96%B9/31%E3%80%80%E8%8B%B1%E9%9B%84%EF%BC%8C%E5%8F%88%E5%90%8D%E5%AF%A6%E9%A9%97%E5%B0%8F%E8%B1%9A%E9%BC%A0.txt)
- [32　再見了我們的（自稱）主人公們](00010_2%E7%AB%A0%20%E3%80%80%E6%AD%A3%E3%81%97%E3%81%84%E5%A4%A9%E4%BD%BF%E3%81%AE%E8%82%B2%E3%81%A6%E6%96%B9/32%E3%80%80%E5%86%8D%E8%A6%8B%E4%BA%86%E6%88%91%E5%80%91%E7%9A%84%EF%BC%88%E8%87%AA%E7%A8%B1%EF%BC%89%E4%B8%BB%E4%BA%BA%E5%85%AC%E5%80%91.txt)
- [33　我還只是，渺小的生物](00010_2%E7%AB%A0%20%E3%80%80%E6%AD%A3%E3%81%97%E3%81%84%E5%A4%A9%E4%BD%BF%E3%81%AE%E8%82%B2%E3%81%A6%E6%96%B9/33%E3%80%80%E6%88%91%E9%82%84%E5%8F%AA%E6%98%AF%EF%BC%8C%E6%B8%BA%E5%B0%8F%E7%9A%84%E7%94%9F%E7%89%A9.txt)
- [34　汚濁](00010_2%E7%AB%A0%20%E3%80%80%E6%AD%A3%E3%81%97%E3%81%84%E5%A4%A9%E4%BD%BF%E3%81%AE%E8%82%B2%E3%81%A6%E6%96%B9/34%E3%80%80%E6%B1%9A%E6%BF%81.txt)
- [35　魔女狩り](00010_2%E7%AB%A0%20%E3%80%80%E6%AD%A3%E3%81%97%E3%81%84%E5%A4%A9%E4%BD%BF%E3%81%AE%E8%82%B2%E3%81%A6%E6%96%B9/35%E3%80%80%E9%AD%94%E5%A5%B3%E7%8B%A9%E3%82%8A.txt)
- [36　獸們的送葬行列](00010_2%E7%AB%A0%20%E3%80%80%E6%AD%A3%E3%81%97%E3%81%84%E5%A4%A9%E4%BD%BF%E3%81%AE%E8%82%B2%E3%81%A6%E6%96%B9/36%E3%80%80%E7%8D%B8%E5%80%91%E7%9A%84%E9%80%81%E8%91%AC%E8%A1%8C%E5%88%97.txt)
- [37　天空與奈落的狹縫間的灰色](00010_2%E7%AB%A0%20%E3%80%80%E6%AD%A3%E3%81%97%E3%81%84%E5%A4%A9%E4%BD%BF%E3%81%AE%E8%82%B2%E3%81%A6%E6%96%B9/37%E3%80%80%E5%A4%A9%E7%A9%BA%E8%88%87%E5%A5%88%E8%90%BD%E7%9A%84%E7%8B%B9%E7%B8%AB%E9%96%93%E7%9A%84%E7%81%B0%E8%89%B2.txt)
- [38　聖女の帰還](00010_2%E7%AB%A0%20%E3%80%80%E6%AD%A3%E3%81%97%E3%81%84%E5%A4%A9%E4%BD%BF%E3%81%AE%E8%82%B2%E3%81%A6%E6%96%B9/38%E3%80%80%E8%81%96%E5%A5%B3%E3%81%AE%E5%B8%B0%E9%82%84.txt)
- [39　堕天](00010_2%E7%AB%A0%20%E3%80%80%E6%AD%A3%E3%81%97%E3%81%84%E5%A4%A9%E4%BD%BF%E3%81%AE%E8%82%B2%E3%81%A6%E6%96%B9/39%E3%80%80%E5%A0%95%E5%A4%A9.txt)
- [40　人格面具的內側](00010_2%E7%AB%A0%20%E3%80%80%E6%AD%A3%E3%81%97%E3%81%84%E5%A4%A9%E4%BD%BF%E3%81%AE%E8%82%B2%E3%81%A6%E6%96%B9/40%E3%80%80%E4%BA%BA%E6%A0%BC%E9%9D%A2%E5%85%B7%E7%9A%84%E5%85%A7%E5%81%B4.txt)
- [41　誕生日](00010_2%E7%AB%A0%20%E3%80%80%E6%AD%A3%E3%81%97%E3%81%84%E5%A4%A9%E4%BD%BF%E3%81%AE%E8%82%B2%E3%81%A6%E6%96%B9/41%E3%80%80%E8%AA%95%E7%94%9F%E6%97%A5.txt)


## [3章 　旅路は血縁に導かれ](00020_3%E7%AB%A0%20%E3%80%80%E6%97%85%E8%B7%AF%E3%81%AF%E8%A1%80%E7%B8%81%E3%81%AB%E5%B0%8E%E3%81%8B%E3%82%8C)

- [42　奧利哈魯鋼是非常美妙的物質](00020_3%E7%AB%A0%20%E3%80%80%E6%97%85%E8%B7%AF%E3%81%AF%E8%A1%80%E7%B8%81%E3%81%AB%E5%B0%8E%E3%81%8B%E3%82%8C/42%E3%80%80%E5%A5%A7%E5%88%A9%E5%93%88%E9%AD%AF%E9%8B%BC%E6%98%AF%E9%9D%9E%E5%B8%B8%E7%BE%8E%E5%A6%99%E7%9A%84%E7%89%A9%E8%B3%AA.txt)
- [43　毒を喰らわば皿まで](00020_3%E7%AB%A0%20%E3%80%80%E6%97%85%E8%B7%AF%E3%81%AF%E8%A1%80%E7%B8%81%E3%81%AB%E5%B0%8E%E3%81%8B%E3%82%8C/43%E3%80%80%E6%AF%92%E3%82%92%E5%96%B0%E3%82%89%E3%82%8F%E3%81%B0%E7%9A%BF%E3%81%BE%E3%81%A7.txt)
- [44　黒白の町](00020_3%E7%AB%A0%20%E3%80%80%E6%97%85%E8%B7%AF%E3%81%AF%E8%A1%80%E7%B8%81%E3%81%AB%E5%B0%8E%E3%81%8B%E3%82%8C/44%E3%80%80%E9%BB%92%E7%99%BD%E3%81%AE%E7%94%BA.txt)
- [45　正義の不在](00020_3%E7%AB%A0%20%E3%80%80%E6%97%85%E8%B7%AF%E3%81%AF%E8%A1%80%E7%B8%81%E3%81%AB%E5%B0%8E%E3%81%8B%E3%82%8C/45%E3%80%80%E6%AD%A3%E7%BE%A9%E3%81%AE%E4%B8%8D%E5%9C%A8.txt)
- [46　殺人鬼は善悪の彼岸で笑う](00020_3%E7%AB%A0%20%E3%80%80%E6%97%85%E8%B7%AF%E3%81%AF%E8%A1%80%E7%B8%81%E3%81%AB%E5%B0%8E%E3%81%8B%E3%82%8C/46%E3%80%80%E6%AE%BA%E4%BA%BA%E9%AC%BC%E3%81%AF%E5%96%84%E6%82%AA%E3%81%AE%E5%BD%BC%E5%B2%B8%E3%81%A7%E7%AC%91%E3%81%86.txt)
- [47　二進制謀殺](00020_3%E7%AB%A0%20%E3%80%80%E6%97%85%E8%B7%AF%E3%81%AF%E8%A1%80%E7%B8%81%E3%81%AB%E5%B0%8E%E3%81%8B%E3%82%8C/47%E3%80%80%E4%BA%8C%E9%80%B2%E5%88%B6%E8%AC%80%E6%AE%BA.txt)
- [48　剛好的殺戮](00020_3%E7%AB%A0%20%E3%80%80%E6%97%85%E8%B7%AF%E3%81%AF%E8%A1%80%E7%B8%81%E3%81%AB%E5%B0%8E%E3%81%8B%E3%82%8C/48%E3%80%80%E5%89%9B%E5%A5%BD%E7%9A%84%E6%AE%BA%E6%88%AE.txt)
- [49　接觸](00020_3%E7%AB%A0%20%E3%80%80%E6%97%85%E8%B7%AF%E3%81%AF%E8%A1%80%E7%B8%81%E3%81%AB%E5%B0%8E%E3%81%8B%E3%82%8C/49%E3%80%80%E6%8E%A5%E8%A7%B8.txt)
- [50　唯一的高超辦法](00020_3%E7%AB%A0%20%E3%80%80%E6%97%85%E8%B7%AF%E3%81%AF%E8%A1%80%E7%B8%81%E3%81%AB%E5%B0%8E%E3%81%8B%E3%82%8C/50%E3%80%80%E5%94%AF%E4%B8%80%E7%9A%84%E9%AB%98%E8%B6%85%E8%BE%A6%E6%B3%95.txt)
- [51　為了終將到來的未來](00020_3%E7%AB%A0%20%E3%80%80%E6%97%85%E8%B7%AF%E3%81%AF%E8%A1%80%E7%B8%81%E3%81%AB%E5%B0%8E%E3%81%8B%E3%82%8C/51%E3%80%80%E7%82%BA%E4%BA%86%E7%B5%82%E5%B0%87%E5%88%B0%E4%BE%86%E7%9A%84%E6%9C%AA%E4%BE%86.txt)
- [52　於隧道對面的城鎮，等待着你](00020_3%E7%AB%A0%20%E3%80%80%E6%97%85%E8%B7%AF%E3%81%AF%E8%A1%80%E7%B8%81%E3%81%AB%E5%B0%8E%E3%81%8B%E3%82%8C/52%E3%80%80%E6%96%BC%E9%9A%A7%E9%81%93%E5%B0%8D%E9%9D%A2%E7%9A%84%E5%9F%8E%E9%8E%AE%EF%BC%8C%E7%AD%89%E5%BE%85%E7%9D%80%E4%BD%A0.txt)
- [53　心碎之後，仍存何物](00020_3%E7%AB%A0%20%E3%80%80%E6%97%85%E8%B7%AF%E3%81%AF%E8%A1%80%E7%B8%81%E3%81%AB%E5%B0%8E%E3%81%8B%E3%82%8C/53%E3%80%80%E5%BF%83%E7%A2%8E%E4%B9%8B%E5%BE%8C%EF%BC%8C%E4%BB%8D%E5%AD%98%E4%BD%95%E7%89%A9.txt)
- [54　新たな旅立ち](00020_3%E7%AB%A0%20%E3%80%80%E6%97%85%E8%B7%AF%E3%81%AF%E8%A1%80%E7%B8%81%E3%81%AB%E5%B0%8E%E3%81%8B%E3%82%8C/54%E3%80%80%E6%96%B0%E3%81%9F%E3%81%AA%E6%97%85%E7%AB%8B%E3%81%A1.txt)
- [55　在籠牢中反抗的人們](00020_3%E7%AB%A0%20%E3%80%80%E6%97%85%E8%B7%AF%E3%81%AF%E8%A1%80%E7%B8%81%E3%81%AB%E5%B0%8E%E3%81%8B%E3%82%8C/55%E3%80%80%E5%9C%A8%E7%B1%A0%E7%89%A2%E4%B8%AD%E5%8F%8D%E6%8A%97%E7%9A%84%E4%BA%BA%E5%80%91.txt)
- [56　聖典所述道](00020_3%E7%AB%A0%20%E3%80%80%E6%97%85%E8%B7%AF%E3%81%AF%E8%A1%80%E7%B8%81%E3%81%AB%E5%B0%8E%E3%81%8B%E3%82%8C/56%E3%80%80%E8%81%96%E5%85%B8%E6%89%80%E8%BF%B0%E9%81%93.txt)
- [57　羽化](00020_3%E7%AB%A0%20%E3%80%80%E6%97%85%E8%B7%AF%E3%81%AF%E8%A1%80%E7%B8%81%E3%81%AB%E5%B0%8E%E3%81%8B%E3%82%8C/57%E3%80%80%E7%BE%BD%E5%8C%96.txt)
- [58　破除虛假的鏡子](00020_3%E7%AB%A0%20%E3%80%80%E6%97%85%E8%B7%AF%E3%81%AF%E8%A1%80%E7%B8%81%E3%81%AB%E5%B0%8E%E3%81%8B%E3%82%8C/58%E3%80%80%E7%A0%B4%E9%99%A4%E8%99%9B%E5%81%87%E7%9A%84%E9%8F%A1%E5%AD%90.txt)
- [59　優等生の虚像](00020_3%E7%AB%A0%20%E3%80%80%E6%97%85%E8%B7%AF%E3%81%AF%E8%A1%80%E7%B8%81%E3%81%AB%E5%B0%8E%E3%81%8B%E3%82%8C/59%E3%80%80%E5%84%AA%E7%AD%89%E7%94%9F%E3%81%AE%E8%99%9A%E5%83%8F.txt)
- [60　『暴食』](00020_3%E7%AB%A0%20%E3%80%80%E6%97%85%E8%B7%AF%E3%81%AF%E8%A1%80%E7%B8%81%E3%81%AB%E5%B0%8E%E3%81%8B%E3%82%8C/60%E3%80%80%E3%80%8E%E6%9A%B4%E9%A3%9F%E3%80%8F.txt)
- [61　決着](00020_3%E7%AB%A0%20%E3%80%80%E6%97%85%E8%B7%AF%E3%81%AF%E8%A1%80%E7%B8%81%E3%81%AB%E5%B0%8E%E3%81%8B%E3%82%8C/61%E3%80%80%E6%B1%BA%E7%9D%80.txt)
- [62　關於在無法證明我們有血緣關係的這個世界裡抑制肉欲](00020_3%E7%AB%A0%20%E3%80%80%E6%97%85%E8%B7%AF%E3%81%AF%E8%A1%80%E7%B8%81%E3%81%AB%E5%B0%8E%E3%81%8B%E3%82%8C/62%E3%80%80%E9%97%9C%E6%96%BC%E5%9C%A8%E7%84%A1%E6%B3%95%E8%AD%89%E6%98%8E%E6%88%91%E5%80%91%E6%9C%89%E8%A1%80%E7%B7%A3%E9%97%9C%E4%BF%82%E7%9A%84%E9%80%99%E5%80%8B%E4%B8%96%E7%95%8C%E8%A3%A1%E6%8A%91%E5%88%B6%E8%82%89%E6%AC%B2.txt)
- [63　逝去的時間是殘酷的](00020_3%E7%AB%A0%20%E3%80%80%E6%97%85%E8%B7%AF%E3%81%AF%E8%A1%80%E7%B8%81%E3%81%AB%E5%B0%8E%E3%81%8B%E3%82%8C/63%E3%80%80%E9%80%9D%E5%8E%BB%E7%9A%84%E6%99%82%E9%96%93%E6%98%AF%E6%AE%98%E9%85%B7%E7%9A%84.txt)


## [4章 　彼女の居ない一週間](00030_4%E7%AB%A0%20%E3%80%80%E5%BD%BC%E5%A5%B3%E3%81%AE%E5%B1%85%E3%81%AA%E3%81%84%E4%B8%80%E9%80%B1%E9%96%93)

- [64　約定之夜，成就尚遙 - MINUS7](00030_4%E7%AB%A0%20%E3%80%80%E5%BD%BC%E5%A5%B3%E3%81%AE%E5%B1%85%E3%81%AA%E3%81%84%E4%B8%80%E9%80%B1%E9%96%93/64%E3%80%80%E7%B4%84%E5%AE%9A%E4%B9%8B%E5%A4%9C%EF%BC%8C%E6%88%90%E5%B0%B1%E5%B0%9A%E9%81%99%20-%20MINUS7.txt)
- [65　這個世界不存在毫無意義的事件 - MINUS6](00030_4%E7%AB%A0%20%E3%80%80%E5%BD%BC%E5%A5%B3%E3%81%AE%E5%B1%85%E3%81%AA%E3%81%84%E4%B8%80%E9%80%B1%E9%96%93/65%E3%80%80%E9%80%99%E5%80%8B%E4%B8%96%E7%95%8C%E4%B8%8D%E5%AD%98%E5%9C%A8%E6%AF%AB%E7%84%A1%E6%84%8F%E7%BE%A9%E7%9A%84%E4%BA%8B%E4%BB%B6%20-%20MINUS6.txt)
- [66　感染擴大 - MINUS5](00030_4%E7%AB%A0%20%E3%80%80%E5%BD%BC%E5%A5%B3%E3%81%AE%E5%B1%85%E3%81%AA%E3%81%84%E4%B8%80%E9%80%B1%E9%96%93/66%E3%80%80%E6%84%9F%E6%9F%93%E6%93%B4%E5%A4%A7%20-%20MINUS5.txt)
- [67　我們未相交 - MINUS4](00030_4%E7%AB%A0%20%E3%80%80%E5%BD%BC%E5%A5%B3%E3%81%AE%E5%B1%85%E3%81%AA%E3%81%84%E4%B8%80%E9%80%B1%E9%96%93/67%E3%80%80%E6%88%91%E5%80%91%E6%9C%AA%E7%9B%B8%E4%BA%A4%20-%20MINUS4.txt)
- [68　栄華の都 - MINUS3](00030_4%E7%AB%A0%20%E3%80%80%E5%BD%BC%E5%A5%B3%E3%81%AE%E5%B1%85%E3%81%AA%E3%81%84%E4%B8%80%E9%80%B1%E9%96%93/68%E3%80%80%E6%A0%84%E8%8F%AF%E3%81%AE%E9%83%BD%20-%20MINUS3.txt)
- [69　因此，我們相愛了。 - MINUS2](00030_4%E7%AB%A0%20%E3%80%80%E5%BD%BC%E5%A5%B3%E3%81%AE%E5%B1%85%E3%81%AA%E3%81%84%E4%B8%80%E9%80%B1%E9%96%93/69%E3%80%80%E5%9B%A0%E6%AD%A4%EF%BC%8C%E6%88%91%E5%80%91%E7%9B%B8%E6%84%9B%E4%BA%86%E3%80%82%20-%20MINUS2.txt)
- [70　決戦前夜 - MINUS1](00030_4%E7%AB%A0%20%E3%80%80%E5%BD%BC%E5%A5%B3%E3%81%AE%E5%B1%85%E3%81%AA%E3%81%84%E4%B8%80%E9%80%B1%E9%96%93/70%E3%80%80%E6%B1%BA%E6%88%A6%E5%89%8D%E5%A4%9C%20-%20MINUS1.txt)
- [71　沒有救濟的戰場 - ZERO the first volume](00030_4%E7%AB%A0%20%E3%80%80%E5%BD%BC%E5%A5%B3%E3%81%AE%E5%B1%85%E3%81%AA%E3%81%84%E4%B8%80%E9%80%B1%E9%96%93/71%E3%80%80%E6%B2%92%E6%9C%89%E6%95%91%E6%BF%9F%E7%9A%84%E6%88%B0%E5%A0%B4%20-%20ZERO%20the%20first%20volume.txt)
- [72　不講道理的奇蹟 - ZERO the second volume](00030_4%E7%AB%A0%20%E3%80%80%E5%BD%BC%E5%A5%B3%E3%81%AE%E5%B1%85%E3%81%AA%E3%81%84%E4%B8%80%E9%80%B1%E9%96%93/72%E3%80%80%E4%B8%8D%E8%AC%9B%E9%81%93%E7%90%86%E7%9A%84%E5%A5%87%E8%B9%9F%20-%20ZERO%20the%20second%20volume.txt)
- [73　蒙受並吞噬絕望 - ZERO the last volume](00030_4%E7%AB%A0%20%E3%80%80%E5%BD%BC%E5%A5%B3%E3%81%AE%E5%B1%85%E3%81%AA%E3%81%84%E4%B8%80%E9%80%B1%E9%96%93/73%E3%80%80%E8%92%99%E5%8F%97%E4%B8%A6%E5%90%9E%E5%99%AC%E7%B5%95%E6%9C%9B%20-%20ZERO%20the%20last%20volume.txt)
- [74　因此，稍稍離別一會兒。 - ZERO overtime](00030_4%E7%AB%A0%20%E3%80%80%E5%BD%BC%E5%A5%B3%E3%81%AE%E5%B1%85%E3%81%AA%E3%81%84%E4%B8%80%E9%80%B1%E9%96%93/74%E3%80%80%E5%9B%A0%E6%AD%A4%EF%BC%8C%E7%A8%8D%E7%A8%8D%E9%9B%A2%E5%88%A5%E4%B8%80%E6%9C%83%E5%85%92%E3%80%82%20-%20ZERO%20overtime.txt)


## [5章 　復讐に例外などない](00040_5%E7%AB%A0%20%E3%80%80%E5%BE%A9%E8%AE%90%E3%81%AB%E4%BE%8B%E5%A4%96%E3%81%AA%E3%81%A9%E3%81%AA%E3%81%84)

- [75　再会と再開](00040_5%E7%AB%A0%20%E3%80%80%E5%BE%A9%E8%AE%90%E3%81%AB%E4%BE%8B%E5%A4%96%E3%81%AA%E3%81%A9%E3%81%AA%E3%81%84/75%E3%80%80%E5%86%8D%E4%BC%9A%E3%81%A8%E5%86%8D%E9%96%8B.txt)
- [76　天國之門](00040_5%E7%AB%A0%20%E3%80%80%E5%BE%A9%E8%AE%90%E3%81%AB%E4%BE%8B%E5%A4%96%E3%81%AA%E3%81%A9%E3%81%AA%E3%81%84/76%E3%80%80%E5%A4%A9%E5%9C%8B%E4%B9%8B%E9%96%80.txt)
- [77　我們大概是朋友吧](00040_5%E7%AB%A0%20%E3%80%80%E5%BE%A9%E8%AE%90%E3%81%AB%E4%BE%8B%E5%A4%96%E3%81%AA%E3%81%A9%E3%81%AA%E3%81%84/77%E3%80%80%E6%88%91%E5%80%91%E5%A4%A7%E6%A6%82%E6%98%AF%E6%9C%8B%E5%8F%8B%E5%90%A7.txt)
- [78　再次開始](00040_5%E7%AB%A0%20%E3%80%80%E5%BE%A9%E8%AE%90%E3%81%AB%E4%BE%8B%E5%A4%96%E3%81%AA%E3%81%A9%E3%81%AA%E3%81%84/78%E3%80%80%E5%86%8D%E6%AC%A1%E9%96%8B%E5%A7%8B.txt)
- [79　播種的人](00040_5%E7%AB%A0%20%E3%80%80%E5%BE%A9%E8%AE%90%E3%81%AB%E4%BE%8B%E5%A4%96%E3%81%AA%E3%81%A9%E3%81%AA%E3%81%84/79%E3%80%80%E6%92%AD%E7%A8%AE%E7%9A%84%E4%BA%BA.txt)
- [80　活死人在笑](00040_5%E7%AB%A0%20%E3%80%80%E5%BE%A9%E8%AE%90%E3%81%AB%E4%BE%8B%E5%A4%96%E3%81%AA%E3%81%A9%E3%81%AA%E3%81%84/80%E3%80%80%E6%B4%BB%E6%AD%BB%E4%BA%BA%E5%9C%A8%E7%AC%91.txt)
- [81　傲慢なる皇帝](00040_5%E7%AB%A0%20%E3%80%80%E5%BE%A9%E8%AE%90%E3%81%AB%E4%BE%8B%E5%A4%96%E3%81%AA%E3%81%A9%E3%81%AA%E3%81%84/81%E3%80%80%E5%82%B2%E6%85%A2%E3%81%AA%E3%82%8B%E7%9A%87%E5%B8%9D.txt)
- [82　發芽](00040_5%E7%AB%A0%20%E3%80%80%E5%BE%A9%E8%AE%90%E3%81%AB%E4%BE%8B%E5%A4%96%E3%81%AA%E3%81%A9%E3%81%AA%E3%81%84/82%E3%80%80%E7%99%BC%E8%8A%BD.txt)
- [83　美麗的花朵綻放吧](00040_5%E7%AB%A0%20%E3%80%80%E5%BE%A9%E8%AE%90%E3%81%AB%E4%BE%8B%E5%A4%96%E3%81%AA%E3%81%A9%E3%81%AA%E3%81%84/83%E3%80%80%E7%BE%8E%E9%BA%97%E7%9A%84%E8%8A%B1%E6%9C%B5%E7%B6%BB%E6%94%BE%E5%90%A7.txt)
- [84　兩條人魚和腐爛屍體的水族館](00040_5%E7%AB%A0%20%E3%80%80%E5%BE%A9%E8%AE%90%E3%81%AB%E4%BE%8B%E5%A4%96%E3%81%AA%E3%81%A9%E3%81%AA%E3%81%84/84%E3%80%80%E5%85%A9%E6%A2%9D%E4%BA%BA%E9%AD%9A%E5%92%8C%E8%85%90%E7%88%9B%E5%B1%8D%E9%AB%94%E7%9A%84%E6%B0%B4%E6%97%8F%E9%A4%A8.txt)
- [85　第３６名死者的慶祝派對](00040_5%E7%AB%A0%20%E3%80%80%E5%BE%A9%E8%AE%90%E3%81%AB%E4%BE%8B%E5%A4%96%E3%81%AA%E3%81%A9%E3%81%AA%E3%81%84/85%E3%80%80%E7%AC%AC%EF%BC%93%EF%BC%96%E5%90%8D%E6%AD%BB%E8%80%85%E7%9A%84%E6%85%B6%E7%A5%9D%E6%B4%BE%E5%B0%8D.txt)
- [86　之所以是天才](00040_5%E7%AB%A0%20%E3%80%80%E5%BE%A9%E8%AE%90%E3%81%AB%E4%BE%8B%E5%A4%96%E3%81%AA%E3%81%A9%E3%81%AA%E3%81%84/86%E3%80%80%E4%B9%8B%E6%89%80%E4%BB%A5%E6%98%AF%E5%A4%A9%E6%89%8D.txt)
- [87　凡人の旅路](00040_5%E7%AB%A0%20%E3%80%80%E5%BE%A9%E8%AE%90%E3%81%AB%E4%BE%8B%E5%A4%96%E3%81%AA%E3%81%A9%E3%81%AA%E3%81%84/87%E3%80%80%E5%87%A1%E4%BA%BA%E3%81%AE%E6%97%85%E8%B7%AF.txt)
- [88　彼女と彼女の適切的自殺願望](00040_5%E7%AB%A0%20%E3%80%80%E5%BE%A9%E8%AE%90%E3%81%AB%E4%BE%8B%E5%A4%96%E3%81%AA%E3%81%A9%E3%81%AA%E3%81%84/88%E3%80%80%E5%BD%BC%E5%A5%B3%E3%81%A8%E5%BD%BC%E5%A5%B3%E3%81%AE%E9%81%A9%E5%88%87%E7%9A%84%E8%87%AA%E6%AE%BA%E9%A1%98%E6%9C%9B.txt)
- [89　３７](00040_5%E7%AB%A0%20%E3%80%80%E5%BE%A9%E8%AE%90%E3%81%AB%E4%BE%8B%E5%A4%96%E3%81%AA%E3%81%A9%E3%81%AA%E3%81%84/89%E3%80%80%EF%BC%93%EF%BC%97.txt)
- [90　我先走一步去等你呢](00040_5%E7%AB%A0%20%E3%80%80%E5%BE%A9%E8%AE%90%E3%81%AB%E4%BE%8B%E5%A4%96%E3%81%AA%E3%81%A9%E3%81%AA%E3%81%84/90%E3%80%80%E6%88%91%E5%85%88%E8%B5%B0%E4%B8%80%E6%AD%A5%E5%8E%BB%E7%AD%89%E4%BD%A0%E5%91%A2.txt)


## [最終章　天国でまた会いましょう](00050_%E6%9C%80%E7%B5%82%E7%AB%A0%E3%80%80%E5%A4%A9%E5%9B%BD%E3%81%A7%E3%81%BE%E3%81%9F%E4%BC%9A%E3%81%84%E3%81%BE%E3%81%97%E3%82%87%E3%81%86)

- [91　燃起狼煙](00050_%E6%9C%80%E7%B5%82%E7%AB%A0%E3%80%80%E5%A4%A9%E5%9B%BD%E3%81%A7%E3%81%BE%E3%81%9F%E4%BC%9A%E3%81%84%E3%81%BE%E3%81%97%E3%82%87%E3%81%86/91%E3%80%80%E7%87%83%E8%B5%B7%E7%8B%BC%E7%85%99.txt)
- [92　一騎當千亦不夠](00050_%E6%9C%80%E7%B5%82%E7%AB%A0%E3%80%80%E5%A4%A9%E5%9B%BD%E3%81%A7%E3%81%BE%E3%81%9F%E4%BC%9A%E3%81%84%E3%81%BE%E3%81%97%E3%82%87%E3%81%86/92%E3%80%80%E4%B8%80%E9%A8%8E%E7%95%B6%E5%8D%83%E4%BA%A6%E4%B8%8D%E5%A4%A0.txt)
- [93　重生的話想做甚麼？](00050_%E6%9C%80%E7%B5%82%E7%AB%A0%E3%80%80%E5%A4%A9%E5%9B%BD%E3%81%A7%E3%81%BE%E3%81%9F%E4%BC%9A%E3%81%84%E3%81%BE%E3%81%97%E3%82%87%E3%81%86/93%E3%80%80%E9%87%8D%E7%94%9F%E7%9A%84%E8%A9%B1%E6%83%B3%E5%81%9A%E7%94%9A%E9%BA%BC%EF%BC%9F.txt)
- [94　繭の園](00050_%E6%9C%80%E7%B5%82%E7%AB%A0%E3%80%80%E5%A4%A9%E5%9B%BD%E3%81%A7%E3%81%BE%E3%81%9F%E4%BC%9A%E3%81%84%E3%81%BE%E3%81%97%E3%82%87%E3%81%86/94%E3%80%80%E7%B9%AD%E3%81%AE%E5%9C%92.txt)
- [95　連繫此岸與彼岸之物](00050_%E6%9C%80%E7%B5%82%E7%AB%A0%E3%80%80%E5%A4%A9%E5%9B%BD%E3%81%A7%E3%81%BE%E3%81%9F%E4%BC%9A%E3%81%84%E3%81%BE%E3%81%97%E3%82%87%E3%81%86/95%E3%80%80%E9%80%A3%E7%B9%AB%E6%AD%A4%E5%B2%B8%E8%88%87%E5%BD%BC%E5%B2%B8%E4%B9%8B%E7%89%A9.txt)
- [96　最後の一手](00050_%E6%9C%80%E7%B5%82%E7%AB%A0%E3%80%80%E5%A4%A9%E5%9B%BD%E3%81%A7%E3%81%BE%E3%81%9F%E4%BC%9A%E3%81%84%E3%81%BE%E3%81%97%E3%82%87%E3%81%86/96%E3%80%80%E6%9C%80%E5%BE%8C%E3%81%AE%E4%B8%80%E6%89%8B.txt)
- [97　來，讓美好結局開始吧](00050_%E6%9C%80%E7%B5%82%E7%AB%A0%E3%80%80%E5%A4%A9%E5%9B%BD%E3%81%A7%E3%81%BE%E3%81%9F%E4%BC%9A%E3%81%84%E3%81%BE%E3%81%97%E3%82%87%E3%81%86/97%E3%80%80%E4%BE%86%EF%BC%8C%E8%AE%93%E7%BE%8E%E5%A5%BD%E7%B5%90%E5%B1%80%E9%96%8B%E5%A7%8B%E5%90%A7.txt)
- [98　然後復讐譚落下帷幕](00050_%E6%9C%80%E7%B5%82%E7%AB%A0%E3%80%80%E5%A4%A9%E5%9B%BD%E3%81%A7%E3%81%BE%E3%81%9F%E4%BC%9A%E3%81%84%E3%81%BE%E3%81%97%E3%82%87%E3%81%86/98%E3%80%80%E7%84%B6%E5%BE%8C%E5%BE%A9%E8%AE%90%E8%AD%9A%E8%90%BD%E4%B8%8B%E5%B8%B7%E5%B9%95.txt)


## [エピローグ](00060_%E3%82%A8%E3%83%94%E3%83%AD%E3%83%BC%E3%82%B0)

- [99　復仇是抓住幸福未來的手段](00060_%E3%82%A8%E3%83%94%E3%83%AD%E3%83%BC%E3%82%B0/99%E3%80%80%E5%BE%A9%E4%BB%87%E6%98%AF%E6%8A%93%E4%BD%8F%E5%B9%B8%E7%A6%8F%E6%9C%AA%E4%BE%86%E7%9A%84%E6%89%8B%E6%AE%B5.txt)
- [100　異世界の旅は希望と共に](00060_%E3%82%A8%E3%83%94%E3%83%AD%E3%83%BC%E3%82%B0/100%E3%80%80%E7%95%B0%E4%B8%96%E7%95%8C%E3%81%AE%E6%97%85%E3%81%AF%E5%B8%8C%E6%9C%9B%E3%81%A8%E5%85%B1%E3%81%AB.txt)

