 第一個朋友的故事?

在日本平成年當這二流公司的普通社員的我,不知道為什麼成為了像西方幻想世界風格裡的零散貴族的八男.

而且有魔法的才能,經過偷偷摸摸地學習和訓練, 進行著貧困的飲食和狩獵生活.

隨著逐漸成長的魔法,與雖然死後變成不死族,但是原本身為人類大貴族僱傭的魔法使的師傅相遇並並順利得取得真傳和繼承了他的遺產.

為了防止繼承競爭所引發的無端糾紛, 我也甘願接受"根本沒有幫家裡忙的任性兒子"這樣的評價,所以我成功得到了行動自由.

首先, 探索著具有無比的寬度而自豪的未開發地, 順便了學習了飛翔魔法和瞬間移動, 已經得到夠玩好幾次人生的資產和素材了.

順便學了製造味增和醬油等的釀造魔法, 實際上,秘密是這種魔法才是最困難的.

領地內的一個地主,想讓我像小歷史劇一樣以下克上般的成為下任領主.

當然, 因為不想做那樣辛苦的事, 我鄭重拒絕了.

在家裡也很有可能有其他以下犯上的勸誘, 十二歲就在其他城市的冒險者預備學校就讀了.

到這裡為止,經過了六年零幾個月的歲月流逝. 通過自己各種各樣的努力, 時間也感覺過得很快.

現在的季節是四月上旬,和前世完全一樣,所以感覺不到任何違和感,不過,這個世界的日曆長度和記數單位也和日本的基本相同.

一年有十二個月, 每個月都是三十一天, 三百七十二天的不同程度呢?

長度由毫米、釐米、米、公里.

重量由克, 千克, 噸.

時間由秒,分,小時.

一個星期從週一到週日, 基本上星期六,日是休息日.

虔誠的神的信徒當天會去教會等地方.

信仰神的人認為,創造了這個世界的神,名為神的人, 用其他名字來稱呼是荒謬的.

(信仰されている神は、この世界を作った神とされていて、名前は神その物で、他の名前など付けるのは言語道斷らしい。)

完全只有這一神教, 至少在這個磷蓋亞大陸(リンガイア大陸)是不存在的.

不管在那個世界都有那種,根據每個地方當地原始的宗教和微妙不同的教義(地元の原始宗教と結び付いて微妙に教義が違ったり),實際上在悠久的歷史中存在的幾個互相排斥的宗教.

還有, 我所居住的鄉下地方的話, 在安息日也沒有多餘的想法呢, 因為每天的農活之後一有空閒的時間就去狩獵和採集, 在哪裡休息之類的想法.

農閒期有比較多的休息時間,不過,我們村就開墾和治水工程就有很多不好的評了.

閒話就不扯了(你也知道), 我平安無事地入學了在普萊西布魯克的冒險者預備學校. 朋友都做不到,開始授課的幾天後的下午, 終於也習慣了預備學校而開始打工的事.

(余計な話が長くなったが、俺が無事にブライヒブルクにある冒険者予備校に入學し、友人も出來、早速授業などが始まってから數日後の午後、ようやく予備校にも慣れ始めたのでアルバイトを始める事にする。)

所以,為了練習戰鬥技能而兼顧著打工和狩獵的事.

和這個世界首次出現的同年齡的朋友, 埃爾文・馮・阿魯尼穆一起.

「啊啦~~~ 終於到了啊」

「沒辦法,附近的狩獵場地已經被其他人佔領了」

我和埃爾, 到了距離預備學校需要步行約一個小時左右的距離的草原.

普萊西布魯克是以二十萬人口為自豪的大城市, 但是在如此人口的影響下,大量的食物是必要的.

穀物或蔬菜從附近很多村子來, 魚類不湊巧地由於大海距離這裡幾百公里遠,所以以川魚為主,也有用鹽醃製的魚乾.

鹽也有點貴, 但是由於被大量的送到這裡,所以比其他的內陸城市更加便宜一些.

砂糖呢, 因為產地在南方, 很便宜所以我也入手了.

然後剩下來肉類呢, 這是周邊地區的農村正在進行的蓄牧業量根本不夠.

經常進行農田的開墾的話糧食產量也會上升了,但是人口比例也會逐漸增加,肉類的生產上使用穀物的量也就追上來了。

因此, 冒險者是重要的存在. 對於冒險者而言進入魔物居住的區域並狩獵魔物,得到貴重的素材和肉這種想法也有, 並沒有想要狩獵所有魔物.

其中有大多數人, 就這樣去了遠離人煙的地方以確保食物的肉. 鄉下的農村有安排專門的獵刃和農民在空閒時間進行狩獵, 有時候全村人一起參加狩獵以得到所需要的肉.

所以, 關於冒險者預備校的學生的打工呢, 有的人是為了占卜自己將來重要的本職工作在這裡狩獵.

野生的魔物雖然沒那麼強,但是偶爾被熊或者狼襲擊而死的冒險者絡繹不絕,疏忽大意的話還是一樣很危險.

不過對於狩獵而言, 應該就有所鬆懈了.

「大家,都慌慌張張的去了附近的狩獵場了呢.」

「太遠的地方,有危險吧」

像狼這些危險的動物,一旦離開人居住的場所就變多了.

而且, 姑且還是學生,所以明天的課也得考慮, 打工組們大半數是朝著街道附近的獵場前進了.

「但是喲,競爭率不會變高麼?」

「實際上,什麼也狩獵不到的傢伙似乎也有很多」

街道附近的獵場經常進行狩獵,理所當然的數量很少,那裡也有專業的冒險者,所以還沒什麼經驗的學生沒有成果的情況更多.

這就是所謂『新人的洗禮』的玩意吧, 就這樣一直持續狩獵了好幾天依然沒有成果的人,在中途放棄了換成了售貨員和搬運行李的人也有很多.

「在這個距離左右,也不太會有其他冒險者啊,威爾」

「安靜一點……」

我和埃爾小聲的說,繼續發動探測魔法來探測周圍

「探測魔法嗎？　好方便使用啊.」

「是方便狩獵的魔法哦。找到了……」

我們兩人向反應的方向移動,看到了在那裡有一隻大野豬正在挖著地下的樹根的場面,應該不會錯,它正在找薯蕷.

「大獵物哦!」

「啊啊~~」

再繼續吵鬧下去的話,凝視也只是無用功,於是我和埃爾馬上準備了弓箭進行瞄準, 埃爾是作為劍技取得了預備學校的特長生, 但其實從小狩獵的緣故, 弓的處理也很擅長.

手藝大概比我用魔法修正軌道還要做得好.他在這幾年努力的狩獵和販賣,得到了一部分到普萊西布魯克為止的旅費和滯留費.

「Boost箭矢」(強化?參考乳龍帝嗎-.-)

「啊啊~~」

下一個瞬間,我和埃爾同時放箭,於是兩支箭深深的扎進了豬的屁股和後背上了.

「強化真是方便啊」

風魔法強化了了箭矢,使飛行距離更長,貫通力上升深深的刺入了獵物, 如果能很好的刺中要害,即使是相當的大人物也有可能實現一擊瀕死.

這次由於獵物撞在了洞口所以沒有太大的傷害.



「嚇了一跳逃了嗎？」

「可惜,是非常生氣.」

由於我前世不懂關於狩獵的事, 在這個世界生活的野生動物, 凶暴個體挺多的.

中了箭,在這裡逃跑是常識吧, 但是不知道為何卻是勃然大怒, 想給自己施加危害的人實施報復.

野豬受了傷,在逆襲的突進中受了重傷搞不好會死的冒險者,一年有幾名發生的事情預備學校的老師不斷的講著.

「突進過來了哦」

「倒不如說,正合我意」

我和埃爾,不慌不忙的放出第二支箭, 同時強化推進了箭矢, 兩支都進入了朝這邊突進的野豬頭頂上

「死了嗎？」

埃爾慎重地靠近野豬,用劍捅著野豬確認著

「很好的開頭,但是,威爾弓也很好吶」

「練習的成果」

起初狙擊微妙幾乎都是用魔法修正軌道弄到了,但是最近終於能狙擊到正確了.

「威爾,使用魔法就好了嘛,不用管我」

「我知道了」

我馬上將死掉的野豬裝進魔法袋裡,用魔法袋的話,結束期間時間會停止狀態,所以不會有野豬的血凝固或者肉質劣化的事.

(俺は、すぐに絕命している豬を魔法の袋に仕舞う。

魔法の袋に仕舞えば、仕舞っている間は時間が止まっている狀態なので豬の血が固まったり肉質が劣化する事もない)

獵物的處理放在以後處理效率會更好, 現在只需要裝進魔法袋就好了.

還有, 現在處理獵物的是我新製作的袋子,是練習魔道具時製作的東西,考慮到野豬滴著血的屍體而事先準備好的魔法袋.

這個新的袋子,即使是一般人使用一般的東西是造不出來的.同樣是一種除了魔法師之外都不能使用的.

簡単に作ったので収容量が家一軒分くらいしかないのが難點であったが、獲物用の袋として割り切れば使い勝手は良いはずであった。

「一公里範圍內,散佈著相當數量的小獵物吶」

「誒~,真的嗎,那我們來比賽誰狩獵多一點」

「輸的人負責做晚飯」

「瞭解!」

我和埃爾開始分開兩路各自追逐獵物, 然後在兩小時後匯合的我們,立刻就發表結果了.

「我有六隻兔子吶.」

「好厲害啊」

「只限定兔子是正確的啊」

　果然埃爾弓的技巧也一樣很出色.

「我有兩隻兔子和三隻holoholo鳥(ホロホロ鳥),嗚~~~啊,輸了啊.」

「數量上的吶.但是你啊,可是抓了好幾隻holoholo鳥吶.」

就算使用弓的技巧有多麼優秀,也有很多因為holoholo鳥對人的的氣息很敏感而在進入弓的射程範圍之前就逃跑的事.從這點來說獵人們也淚目.

我是用了魔法改變弓的射程和軌道,所以比較簡單就捕獲了.

「數量上是埃爾的勝利,你想吃什麼?」

「回到街上再做決定吧,怎麼了嗎?」

「靠近街道往東五百米處,有兩個人類的反應,狼一樣的反應為十二麼.....」

「不是很熟練的嗎？」

「啊啊~」

現在的情況是, 狼群包圍了來狩獵的兩個人.狗的同伴成群的狼,個體也好群體也好都威脅著人類. 實際上,每年有很多人因為被狼襲擊而丟掉性命.

「去幫忙嗎?」

「回去的路上,死了的話會睡不著覺的」

「但是,來得及嗎？」

「不管了,緊急手段」

我馬上用強化身體機能和速度提升的魔法,抱著埃爾以可怕的速度前往現場了.

「你這傢伙! 至少啊,什麼魔法和順序說明一下再走啊!」

「沒有時間了啊,喂,走了啊.」

「啊」

僅僅幾十秒就過了五百米的距離一邊抱著埃爾一邊奔跑的我,一邊聽著埃爾的投訴一邊確認現場的狀況.

在那裡,有兩個和我們一樣的預備學校的學生被狼包圍著.

一個拿著槍, 另一個人是很稀奇的兩手裝備了手甲像拳術師一樣的.

在這個西方幻想世界裡, 其實拳法這種流行戰鬥技巧也普及開來了.

為了即使在戰場上失去武器赤手空拳時也能戰鬥而開發的戰場格鬥術基礎理論,在那之後誕生了很多流派.

但是,到現在大部分都日益衰退了.

果然徒手的話,無論如何也無法對抗凶暴的野生動物,更不用說與魔物對抗了.

一部分的流派是城市維持治安的警備隊等所指定的必須訓練菜單才勉強保住命脈.

還有就是冒險者之間普及的魔鬥流為世間最有名的也說不定.

魔鬥流,如同字面所述,是用魔力和鬥氣改變戰鬥的格鬥術.

理所當然,在某種程度上並不能使用魔力.

被認為厲害的,最低也要初級到中級之間左右的魔力.

只是,標榜著流派的人不一定就是持有必要的魔力,所以,目的只是為了傳達人類的技之型的修練方法也是世間常識.

還有,用魔力來戰鬥, 使用的時候不會使用其他魔法.

而且魔力在中級以下的,能記住的魔法很少,或者說能記住的魔法種類微妙的面向人類社會的認識.

然而,根據魔力的消耗程度的修行來提高效率和減少魔力使用來長時間像超人一樣的戰鬥,實際上在冒險者歷史中也載入了不少稱號的人.

「喂,覺得眼熟嗎？」

「嗯」

雖然這樣說.被狼包圍的兩人,是在預備學校一樣是特長班的同班同學.

使用槍的是與我們年齡相仿的,燃燒著的鮮紅色長髮到腰,毫無做作的纏在後面.

苗條的身材,像貓一樣的印象的美少女,艾娜・蘇珊妮・希倫布蘭德.

她的老家,在當地經營著教授布萊希堡士兵們長槍術的道館.好像是名貴族的樣子,但實際上她的老家是正式的貴族.

由於槍術的本領,被布萊希堡的領主布萊希萊德邊境伯爵從教授士兵們槍術的教師任命為臣.

正式的貴族的話,只適用於王國任命的職務者和其家人.

所以,布萊希萊德邊境伯爵及其家屬是貴族,所以我老家雖然是零星的騎士也算是貴族了.

如果大貴族有其他大身份的親族在的話,收入也會比我們多很多了.但是即便如此,他們這些陪臣也算是正規貴族了.

(その実入りはうちよりも遙かに多かったりするのだが、それでも彼らは陪臣なので正確には貴族ではない。)

服侍貴族領地內的只有貴族處理,半貴族這種待遇.

最近,平民中什麼也不明白的人越來越多了.

但是,誰也沒有任何問題.

而且,就連陪臣孩子們也有共同的悲喜劇

(それに、陪臣でも家を継げない子供の悲喜劇は共通だ。)

的確,這位伊娜・蘇珊妮・希倫布蘭德也以三女為自我介紹.

會嫁到哪裡呢,雖然如此,一般陪臣的三女等也不會嫁入同是陪臣的門下,那麼只好以自身喜好和冒險者為目標了.

其實呢,這種情況的女性冒險者是相當多的.要求有力氣的軍隊對女性的開放十分狹窄,自然也就只能把冒險者作為目標了.

另一個人也和我們一樣是十二歲,嬌小得搞不好其實才十歲左右.

儘管如此,也是因為作為魔鬥流的特長生,應該有相當的能力的美少女

稍微剪短的水藍色頭髮擋住臉,看起來十分可愛,名為露易絲・約蘭德・奧裡利亞・奧弗韋格.

這都能記住的我的記憶力,可能比想像中的還要厲害很多呢.

不過貴族的名字,就連我自身的也是又長又麻煩.

她的老家也是在布萊希堡正在教授著士兵們魔鬥流,聽說是布萊希萊德邊境伯爵的家臣一樣的身世.

和依娜一樣是三女,自身為了成為冒險者的目標,才想起來做了自我介紹.

說實話,預備學校的特長生班級也有很多這種人混在裡面.

當然,普通班級大部分都是.

甚至連貴族也能輕鬆的買賣,這也是這個世界上很好打小算盤的證據了.

多少貴族的孩子,大家都是貴族,即使王國有再多預算和領土也不夠.

所以,內定以外的子孫向著平民化墮落下去.

最近,王族的這種情況也在逐漸增加,在王家出生也絕不能稱得上安泰是常識.

嘛,有說明什麼的空閒的話,實際上在此期間,放下了抱著的埃爾用弓的二連射往兩頭狼的頭上紮了進去瞬間奪取了性命. 剩下十隻用久違的魔法把全部沉默了.

首先用土牆壁魔法將兩名女性從狼身邊隔離,然後連續發射無屬性的魔法箭一口氣把狼群殺光了.



「我的活躍毫無意義啊!或者說,威爾!那個魔法的話,就連箭矢都不需要了嘛!」

「需要,使用弓箭可以節約魔力的使用」

回答著這樣的埃爾邊解除了圍著兩名女性的土牆壁,在那裡的是保持著驚訝表情的兩人.



「那..個、沒問題麼？」

「沒問題不過...你確實是,是同班的溫德林吧?附近的鮑邁斯塔一家的八男.」

偶然間幫助的兩名同班同學的美少女,我到底是否能成為好朋友呢?

或者說,這是我六年之後和同年齡女性的對話.
