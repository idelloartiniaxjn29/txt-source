﻿幕間十二 主人奇怪的指令。

「喂，有什麼事嗎？」

「只有我們兩人的重要商談」

溫爾的生日晚會上了，按預定會在布萊希萊德邊境伯大人的王都的房子裡舉行了。

客人，不僅僅大小的貴族，商人們，到了二百名以上。

布萊希萊德邊境伯先生本人也新奇地要來了，在那之前布蘭塔庫先生也對出席者進行的嚴選。

參加者有ルックナー財務卿、霍恩海姆紅衣主教，愛德格軍務卿，ブルックナー農務卿和層層的人了，所以自己也參加，不想輸給他們吧。

之後……。

「少年！生日快樂啊！」

溫爾命名的『肌肉引導僧』,阿姆斯特朗也出席成為街巷的話題，那樣強烈的向握手，從他手掌離開後是不是變得七零八落了吧。

「從現在開始，一同以魔術師的最高峰作為目標？！」

「導師！好痛！好痛！」

覺得琵琶骨破碎的那樣，肩膀也發出棒棒敲的聲音了。

說起來，溫爾之後悄悄地對掌，肩膀使用治癒魔法。

或許，進入了皸裂左右也說不定。

熱情的問候和禮物攻勢。

溫爾好像很忙的樣子，終於也結束了那個義務，下周就與大家舉行生日派對。

參加者，埃爾，我，露易絲，埃莉斯。

布蘭塔庫先生，阿姆斯特朗導師，アルテリオさん（Arte里約先生），埃裡希先生，保羅先生，赫爾穆特先生。

還有，埃裡希先生的夫人米莉雅姆小姐和勃蘭特夫妻也參加了。

好像Arte里約先生也參加，那一帶政商稱他為，人脈構成力。

「蛋糕，主要是埃莉斯製作。料理，我們和米莉雅姆小姐，夫人也幫我們了」

所謂夫人，是米莉雅姆小姐的事

她婆家也騎士爵家，所以，普通的料理能做到的。

下級貴族家的女性，各種各樣的辛苦的。

（關係複雜，我也不想理清了）

「我們商量下溫爾的禮物。」

雖然本人是以「不需要，有聚會的祝福就足夠了」的說。

但是，大家都有準備。

埃裡希先生，從老家出來之後，每年贈送溫爾禮物，溫爾也每年回禮。

因為他只是下級官吏，金錢也不是充裕的。

儘管如此，能使用的感覺好的毛衣作為便服啦，在王都發現的新奇的魔術關聯的書籍啦

 不是高價的物品,不過，選擇方法非常有感覺，把溫爾也稱為著『這個感覺的好處不能倣傚』。其他的人也在各自考慮著禮物。

「我們要不要也考慮下有衝擊的禮物」

「そういう狙いで暴走して、スベると大変よ

「用那樣的目標亂跑，suberu和非常」、（來助攻）

一定露易絲，意識到埃莉斯了吧。

說起來，埃莉斯是能靈巧的縫製男性用的衣服。

不僅是料理，裁縫也擅長啦。

「什麼，這個完美超人！」露易絲到了喊著的地步。

本人說，「偶爾教會舉辦的慈善義賣，因為在那裡出品和縫衣服了嗎」的回答。

其他，為了孤兒院的孩子們製作衣服，或修理或的事也多。

叫著『教會真是如同所說的不能輕視啊！』。

說不定要成為好的媳婦兒，在教會接受教育就好了。

「在這裡有應該怎麼對抗那樣高得分的埃莉斯的方法！」

一邊說，一邊露易絲是拿了一本很舊的書出來。

從封面上被使用了的皮製裝裱看來，這是一部向少數愛好者出售的產品吧。

舊來看，認為也有古董價值。

但是露易絲，這樣看起來很貴的書是從哪裡得到的吧？

「在哪裡買的？」

「向布萊希萊德邊境伯先生借的。」

前幾天舉行的的生日聚會之後借的。

「什麼樣的書呢？」

「使溫爾對我們入迷的書呢」

說起來，以前從父親聽說布萊希萊德邊境伯大人的唯一的愛好是收集寶貴的古書。

這也一定是那寶貴的收藏之一吧。

那寶貴的收藏，溫爾著迷我們的關聯性不是很清楚。

「但是，我沒有是吝嗇。」

「非常寶貴的經驗,還會再弄不到手」

無論花多少錢也找不到的價值同這本古書存在的東西

「到底什麼樣的書呢？」

一邊說著，一邊看封面。

『女僕們和像野獸一樣的主人的下午』訂正版。

這樣的書，借就足夠了。

「只看標題，有討厭的預感。

「難得布萊希萊德邊境伯先生借給我了」

決定重新振作起來，試試看看內容

但是，布萊希萊德邊境伯真的是讓我讀這本書嗎？

我心中對他冷靜的內政家這樣的印象快要崩潰了。

哦，緊張狀態反過來積存著，出現了說不定這樣的書可行的想法。

「嗯……。「我們的主人愛女僕組合。但是最近，主人也許有點厭倦我們來的』」

標題是那個,不過，希望內容或許是我們所想要的方法。

文章連外行的我查閱起來也很普通。

內容是，偶爾王都禁售發出，命令孩子禁止的小說一樣。

文字漢字也被廣泛使用著，這也許就是這個書質量高的部分的。

推進「讀」。

「嗯……」

內容概括，兩人年輕女僕為了不讓主人厭煩，開始創意的故事。

第一章，超短裙女僕的卷。

第二章,貓耳女僕的卷。

第三章,男裝執事女僕的卷。

第四章,得到受歡迎咖啡店的女服務員服裝！

第五章,最後的手段，夜晚的禮物大作戰

這以後的章節也是，越讀越愚蠢的，所以一旦阻止的。

「非常壯烈，好像笨蛋啊」

男人，這樣的喜歡似的」

問題是，這什麼參考？這個問題。

裙子非常的短的女僕服裝，頭上的裝飾貓的耳朵和屁股修飾尾巴的嗎，女扮男裝，現在也現存王都的受歡迎的咖啡店的制服上得到的？。

「伊娜醬，最後的是很有效的吧。」

「最可恥的吧」

主人的生日，裸體絲帶纏。

『私達が、プ・レ・ゼ・ン・ト』とやると本には書かれていた。

「我~~ 們~~是~~ 禮~~ 物~~」在書上寫著這樣做。（自翻，哪位有更好意見可以說）

現實是，首先這是不可能的情況。

但是，大人物的貴族的話，難道有實際做了的嗎？

漸漸，感到正常的判斷力像變鈍一樣的心情。

普通に恥ずかしいじゃない。というか、やると色々と終ると思うけど……」

「不是普通地害羞。或者說，做的話想……這個那個地結束,不過...」（腦補不夠啊）

溫爾開心的話，我們就贏了吧，吃驚的可能性也是有的。

「但是，布萊雷達邊境伯的書。」

「我說……」

因為對方是當家大人，應該這樣做不會變成什麼問題。

,不過不那樣想的話，就太害羞了而沒理由繼續這樣的實行。

但是，雖說人能做的，布萊雷達邊邊境伯先生也給了可怕的書。

從能送給一族的未婚妻的，值得我們期待的部分大的吧。

「安妮塔大人就按這本書說的那樣」

「停止！」

這樣說主家也許失禮，如果超過四十歲的安妮塔大人在這本書上寫的樣子誘惑溫爾的話，就連韋爾也會生氣。

布萊希萊德邊境伯家靠近孩子停止，泣き付く埃裡希先生也許。

「沒辦法呢……」

悲哀的，反正是臣下之臣的女兒。

我們反抗沒能布萊希萊德邊境伯殿下的命令。

結果対責任問題，連我們都不知道了。

「絲帶的顏色，青是我的，伊娜是紅色。」

「搭配頭髮的顏色的吧……」

真的，怎麼都好啦。

儘管如此，我們那天購入搭配絲帶，花費時間仔細地商量，準備。

「ふぁーーーあ！ 眠っ」

「ふぁ-啊！沉睡」（誰來給我解釋下）

然後，當天堅決進行計畫。

今天舉行了生日派對，溫馨的氣氛快樂地結束了。

大家開心地吃料理，把禮物交給蛋糕溫爾，溫爾站起來吹滅了蠟燭的火。

溫爾也很開心，非常好的聚會呢。

然後，當天的夜晚。

終於，到了實施計畫的時候了。

「好, 進入了溫爾的房間呢」

「魔闘流奧義，能讓氣息消失的妙技，很有用呢。」

感覺寶貴的奧義浪費了，但這以後就韋爾走進房間的伺機準備就好了。

「不害羞。因為我這也是為了溫爾。」

「這樣的藉口也不錯。不過像這樣，傻氣的很開心了」

結束說話的時候，房間的門打開，想睡覺，揉著眼睛的溫爾進來了。

從現在開始，戰鬥開始了。

「嗯……」

裸に、いきなり見えると困る部分にリボンを通し、ヴェルに見え易いように頭の部分で蝶結びをしている私とルイーゼ。

我和ruize，對感到為難的部分，通過絲帶纏繞，以頭的部分做蝴蝶結的。（求幫忙）

溫爾對於突然的出現眼前的事很吃驚。

然後，就這樣只能按計畫做了。

在這裡害羞的話，以後就只會更加不好意思了，那無聊的書也這樣寫的。

倒不如自己解放這件事，才能得到相連的未來的勝利。

「（已經無法回頭！）我~~們~~是~~禮~~物-~~~~~」

兩個人同時說出台詞，好好地賣弄研究的成果的姿勢到給韋爾看。

いくら普段のヴェルが自重して、私達にたまにキスをするくらいでも。

エリーゼの胸を、視線を追われないように瞬間的に見るという、まるで無駄な行動をしていても。

（平時溫爾自重，我們大概偶爾接吻。

視線瞬間被所看到的埃莉斯的胸口奪走，我們就像無用的行動。）

不然也不會形成這個以裸體絲帶的攻擊的策略。

參考的書是有問題，借給我們的人，布萊希萊德邊境伯大人的要求是一樣的。

「（來，如何反應？難道是……）」

也考慮到溫爾的興奮的可能性，但我溫爾會怎樣行動呢，露易絲一起等待的。

於是，突然過來溫爾抱住我。

出乎意料的結局，露易絲也很吃驚。

「溫！溫！溫爾...」

「嗯，我知道。因為明白的」

什麼你知道嗎？不知道，但是溫爾還是繼續說。

「露易絲上挑唆的吧。伊娜自己不會這樣做的」

「咦，隊我這樣的印象？」

韋爾的發言，露易絲很不滿。

「那個，溫爾？」

「老實說，非常興奮。但是，伊娜的魅力是這樣的事不做，也十分的明白。」

「那個……」

「我說……」

挑唆的疑惑，露易絲已經一半放心。

怎麼說呢，這樣的時候我平時的言行モロに影響之類，非常學習了。

實際上，露易絲是主犯的事實確實。

還有，還是韋爾對我的印象，是冷靜的，認真的女人吧。

然後，把這樣的我韋爾可喜的感覺。

說不定稍微從色情脫離,不過說不定對好的合夥人(夫婦)馴熟。

「最近，偉大的人被沖走的，不過我是成人的話伊娜和露易絲都結婚了。但是，這樣勉強是不可以」

韋爾說那樣的話，讓我們穿自己的襯衫和蒙上床的床單，就從房間裡出來了。

之後，留下了我們。

冷靜的話，只有裸體絲帶的打扮果然還是很害羞啊。

還有，布萊希萊德邊境伯以後對我們這麼做得到什麼呢？

我說不定誇大評價布萊希萊德邊境伯大人了吧？

冷靜，想得越多，思緒總是漂浮著。

「哎，這是成功了嗎？」

「也能聽到帶有求婚的發言，所以成功了吧」

也許偶爾嘗試花樣不相合的事物的也不錯呢。

あと、モテないと言いながら、ヴェルが意外と格好良かったのを知ったのは、良い収獲だったと思う私であった。

這以後，一邊說，一邊想知道了的溫爾意外帥的，不受歡迎的我是好的收穫。

「那兩個人的手段，真是可怕的誘惑啊……」

難道這就是裸色帶禮物的發言的攻勢。

逃出來了，我無論如何，都不能輸給誘惑，。

最近，心中也有對埃莉斯的微妙的感覺，請饒了我吧。

「（等到成人再出手！）」

ヴェデリン的內容，這樣想的，所以不打算彎曲。

只是普通的吻把，不過，前世的歐美接吻只是寒暄。

所以，接吻是問候的也延長的這個世界上話，我擅自決定的。

「（對不起，謊言。和非常可愛的女孩子接吻了。）」

這是誰的藉口也不明了，不過，由於之前奇怪的河馬的緣故，和肌肉導師在內的三名男性接吻的時候,從板牆。（詳細請等幕間10）

「但是，成人以後三個夫人不是嗎？。我是人生贏家麼？」

ただし、中身のスケベさで成人後は遠慮しない事を誓うのであった。

早く、成人年齢にならないかなと思いながら。

（但是，這好色的內容, 發誓成人後就不必客氣了。

想快速地到成人年齡啊。）（哪位細翻下）
